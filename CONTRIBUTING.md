How to contribute
=================

Thank you for being here. I'll follow you through a couple steps on how Wasted on Destiny is developed and how *you* can be part of it.

First of all, before you touch the code, here are some resources that you can read if you aren't already familiar with the technologies involved:

- [Javascript](http://devdocs.io/javascript/)
- [Vue.js](https://vuejs.org/v2/guide/)
- [VueX](https://vuex.vuejs.org/)
- [Bulma](https://bulma.io/documentation/)
- [Express](https://expressjs.com)

---

How to submit changes
---------------------

If you feel something could use some improvements, please submit a new issue and ask to have it assigned to yourself. You will gladly be added to the contributors. In the meantime, once your issue is created and detailed, you can immediately start coding. For this, the procedure is the following:

1. Create an issue and note it's number
2. Clone the repository on your computer and checkout the branch `develop`
3. Create a new branch beginning with the issue number, followed by a PascalCase title, like so: `42-AnswerToTheUniverse`
4. Make your changes (and commit/push often to not lose anything!)
5. When you are done, submit a merge request to `develop`

Don't worry too much if your get an email saying your branch failed. It happens because we have continuous integration tasks that build on every push. Make sure your branch passes before merging into develop though!

---

Coding conventions
------------------

Just try to follow what's already there :)

Here's a little preview of how we organize our lines of code:

- Javascript
    - Two space indentation
    - No semi-colons (`;`)
    - Prefer async/await to Promises or callbacks
    - No logic in HTML, except for conditionals and loops
- General
    - API is only Javascript and is completely detached from UI
    - UI is only Javascript and only makes asynchronous queries to the API
    - Don't include big libraries for only a small subset of features that can be solved with a few dozens lines of code
    - Don't reinvent the wheel: if a big problem can be solved with a small library, use it instead of adding thousands of lines of code